#!/bin/bash

if [ $(uname -s) = "Darwin" ]; then
    echo 'Running on unsupported platform - vgk binary may be incompatible'
    sed() { gsed "$@"; }; export -f sed
fi

iter()  {
    # XTU=XTL=0.07
    MACH=$MACHIN"00"
    AOA=$AOAIN"00"
    RE=$(echo "scale=2; x = $REM / 10; if(x<1) print 0; x" | bc )"E+07"
    rm -rf test; cp -a sample test
    mkdir -p results/$FOIL-$FACTOR/$MACHIN-$REM-$AOAIN
    python3 python/geom.py test/run.$FOIL.100.dat test/run.dat $FACTOR
    sed -i $OPT "s/CHGMACH/$MACH/g" test/.script
    sed -i $OPT "s/CHGMACH/$MACH/g" test/*
    sed -i $OPT "s/CHGR/$REM/g" test/.script
    sed -i $OPT "s/CHGRE/$RE/g" test/*
    sed -i $OPT "s/ CHGAOA/"$AOA"/g" test/.script
    sed -i $OPT "s/ CHGAOA/"$AOA"/g" test/*
    cd test; ../bin/vgk < Autorun.SIR
    # mv *.VEH *.SEP *.IEH *.FUL *.BRF run.dat ../results/$FOIL-$FACTOR/$MACHIN-$REM-$AOAIN; cd ..
    mv *.VEH *.SEP *.IEH *.FUL *.BRF ../results/$FOIL-$FACTOR/$MACHIN-$REM-$AOAIN; cd ..
    rm -rf test
}

if [ -z $1 ]; then
    echo "Usage: $0 [a|m|e|t|i]"
elif [ "$1" = "m" ]; then # generates off design mach numbers
    FOIL="report"; FACTOR="1.00"; REM="9.00"; AOAIN="+2.29"
    for j in {1..100}; do
        MACHIN=$(echo "scale=2; x = 0.01*$j; if(x<1) print 0; x" | bc )
        iter
    done
elif [ "$1" = "a" ]; then # generates off design angles
    FOIL="report"; FACTOR="1.00"; REM="9.00"; MACHIN="0.73"
    for i in {1..100}; do
        AOAIN="+$(echo "scale=2; x = 0.04*$i; if(x<1) print 0; x" | bc)"
        iter
        AOAIN="-${AOAIN:1}"
        iter
    done
elif [ "$1" = "e" ]; then # generates off design mach numbers (low resolution)
    FOIL="report"; FACTOR="1.00"; REM="9.00"
    for i in {1..25}; do
        MACHIN=$(echo "scale=2; x = 0.04*$i; if(x<1) print 0; x" | bc )
        for j in {1..25}; do
            AOAIN="+$(echo "scale=2; x = 0.16*$j; if(x<1) print 0; x" | bc)"
            iter
            AOAIN="-${AOAIN:1}"
            iter
        done
    done
elif [ "$1" = "t" ]; then # single test on base foil
    IN="rae5230.12"; BASE="sc20712"
    # IN="rae5230.14"; BASE="sc20714"
    FOIL=$IN.$BASE; FACTOR="1.00"; MACHIN="0.71"; REM="8.00"
    python3 python/reshape.ss.py sample/run.$IN.dat sample/$BASE.dat sample/run.$FOIL.100.dat 
    for i in {1..8}; do
        AOAIN="+$(echo "scale=2; x = 0.25*$i; if(x<1) print 0; x" | bc)" # res 0.50 deg
        MACHIN="0.71"; REM="8.00"; iter
        MACHIN="0.73"; REM="9.00"; iter
        MACHIN="0.75"; REM="10.00"; iter
    done
    # rm -rf results/$FOIL-$FACTOR/
    echo "python3 python/airplot.py results/$FOIL-$FACTOR/$MACHIN-$REM-$AOAIN/Autorun.BRF "
elif [ "$1" = "i" ]; then # iterate through initial cases
    # POTENTIALLY DANGEROUS
    for i in {5..16}; do
        FACTOR=$(echo "scale=2; x = 0.01*$i; if(x<1) print 0; x" | bc) # res 0.01
        for j in {1..5}; do
            AOAIN="+$(echo "scale=2; x = 0.50*$j; if(x<1) print 0; x" | bc)" # res 0.50 deg
            for FOIL in "rae2822" "rae5230"; do
                MACHIN="0.71"; REM="8.00"; iter
                MACHIN="0.73"; REM="9.00"; iter
                MACHIN="0.75"; REM="10.00"; iter
            done
        done
    done
fi
