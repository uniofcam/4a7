#!/bin/bash

factor=0.12
line_number=0

while read line; do
  line_number=$((line_number+1))
  if [ $line_number -gt 3 ]; then
    echo "$(echo "$line" | awk '{print $1}') $(echo "$(echo "$line" | awk '{print $2}')*$factor" | bc -l) $(echo "$line" | awk '{print $3}')"
  fi
done < run.100.dat
