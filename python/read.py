import os
import numpy as np
from airload import airload
from airload import plot_cp
from airload import plot_cp_corr

directory = 'results/'
# foils=["rae2822","rae5230","naca00"]
foils=["report"]
# factors=[ 0.01*i for i in range(1, 21)]
factors=[ 0.01*i for i in range(2, 18)] # ban super thin aerofoils
# machs=["0.71"]; rems=["8.00"]
machrems=["0.71-8.00","0.73-9.00","0.75-10.00"]
aoas=[ "{:.2f}".format(0.50*i) for i in range(2, 18)]
dict = {}

for foil in foils:
    for factor in factors:
        for machrem in machrems:
        # for mach in machs:
            # for rem in rems:
            for aoa in aoas: # do not cycle through AOAs
                # prop = str(foil) + "-" + str(factor) + "/" + str(mach) + "-" + str(rem) + "-" + str(aoa)
                # prop = str(foil) + "-" + str(factor) + "/" + str(mach) + "-" + str(rem) + "-" + "0.50" # 0.50 deg AOA
                prop = str(foil) + "-" + str(factor) + "/" + str(machrem) + "-" + str(aoa); print(prop)
                file = directory + prop + "/Autorun.BRF"
                if os.path.isfile(file):
                    Xupper, CPupper, Xlower, CPlower, em, cl, cdv = airload(file)
                    if cdv['cdv+cd2'] - cdv['cdv'] < 0.0012:
                        mach=float(machrem.split("-")[0])
                        cpstar = 2/1.4/float(mach)/float(mach)*(((2+0.4*float(mach)*float(mach))/2.4)**3.5-1)
                        CPupper = [ max(cpstar, cp) for cp in CPupper ]; CPlower = [ max(cpstar, cp) for cp in CPlower ] # limit max to cpstar
                        cl_corr = np.trapz(CPlower, Xlower) - np.trapz(CPupper, Xupper)
                        dict[prop] = float(cl_corr)

max_prop = list(dict.keys())[0]
for prop in dict:
    if dict[prop] >= dict[max_prop]:
        max_prop = prop
        max_cl = dict[prop]

print("Max CL at {} with {}".format(max_cl, max_prop))
plot_cp(directory + max_prop + "/Autorun.BRF")
