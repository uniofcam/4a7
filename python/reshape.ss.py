import sys
import os

sep = 0.02
in_file = str(sys.argv[1])
base_file = str(sys.argv[2])
if len(sys.argv) < 4:
    out_file = in_file.replace('dat', os.path.basename(base_file))
else:
    out_file = str(sys.argv[3])

def interpolate(coords, x):
    i = 0
    while i < len(coords) - 1 and coords[i+1][0] < x:
        i += 1
    x0, y0 = coords[i]
    x1, y1 = coords[i+1]
    slope = (y1 - y0) / (x1 - x0)
    y = y0 + slope * (x - x0)
    return y

base_columns_ss = []
base_columns_ps = []
with open(base_file, 'r') as base:
    for i, line in enumerate(base):
        if i == 1:
            if len(line.split()) > 1:
                num_points = float(line.split()[1])
        elif i >= 2:
            columns = [float(i) for i in line.split()]
            if columns[0] >= sep and columns[0] <= 1 - 10 * sep: # artificial smoothening
                if len(columns) == 0:
                    True
                elif i - 2 < num_points: # ss
                    base_columns_ss.append(columns)
                elif i - 2 > num_points: # ps
                    base_columns_ps.append(columns)
base_columns_ss.append([1.0, 0.0])

thickness = {}
with open(in_file, 'r') as input:
    for i, line in enumerate(input):
        if i >= 2:
            columns = [float(i) for i in line.split()]
            if len(columns) == 0:
                True
            elif str(columns[0]) not in thickness:
                thickness[str(columns[0])] = columns[1]
            elif str(columns[0]) in thickness:
                thickness[str(columns[0])] = abs(columns[1] - thickness[str(columns[0])])

with open(in_file, 'r') as input:
    with open(out_file, 'w') as output:
        for i, line in enumerate(input):
            if i < 2:
                output.write(line)
                if len(line.split()) > 1:
                    num_points = float(line.split()[1])
            else:
                columns = [float(i) for i in line.split()]
                if columns[0] >= sep * 2 and columns[0] <= 1 - sep: # emulate shipped programme
                    if i - 2 < num_points: # ss
                        columns[1] = interpolate(base_columns_ss, columns[0])
                    elif i - 2 > num_points: # ps
                        print('<point x="{:.6f}" y="{:.6f}" locked="0" />'.format(columns[0],interpolate(base_columns_ss, columns[0]) - 0.5*thickness[str(columns[0])]))
                        columns[1] = interpolate(base_columns_ss, columns[0]) - thickness[str(columns[0])]
                columns = ["{:.6f}".format(i) for i in columns]
                output.write(' '.join(columns) + '\n')
