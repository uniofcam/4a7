import matplotlib.pyplot as plt


def airload(filename):
    """
    Process the values in a file produced by airfoil.
    Assumes Xupper, CPupper, Xlower, CPlower, em, cl and cdv.
    Values are returned into these variables.
    It assumes that "Upper surface CP values" starts a table of values which is
    ended by "Lower surface CP values", which also starts another
    table ended by a blank line.
    Also assumes that em, cl and cdv are set at the start of a line.

    Sample usage:
        global Xupper CPupper Xlower CPlower em cl cdv
        airload('Autorun.BRF')

    Args:
        filename (str): The name of the file to be processed.

    Returns:
        None
    """
    if not filename:
        print("You need to provide a filename")
        return
    
    Xupper = CPupper = Xlower = CPlower = []; em = cl = 0; cdv = {} # handle cases where iterative procedure diverged

    try:
        with open(filename, 'r') as f:
            for line in f:
                if line.startswith(' ITERATIVE PROCEDURE HAS DIVERGED'):
                    break
                elif line.startswith(' EM ='):
                    em = line.split()[2]
                elif line.startswith('   CL ='):
                    cl = float(line.split()[2])
                elif line.startswith('  CDV ='):
                    cdv["cdv"] = float(line.split()[2])
                elif line.startswith('  CDV+CD1 ='):
                    cdv["cdv+cd1"] = float(line.split()[2])
                    cdv["cdv+cd2"] = float(line.split()[5])
                elif line.strip() == 'Upper surface CP values':
                    Xupper = []
                    CPupper = []
                    for line in f:
                        if line.strip() == 'Lower surface CP values':
                            break
                        # elif line.strip() == 'X          CP':
                        elif line.strip() == 'X          CP' or '*' in line:
                            True
                        else:
                            strs = line.split()
                            Xupper.append(float(strs[0]))
                            CPupper.append(float(strs[1]))
                    Xlower = []
                    CPlower = []
                    for line in f:
                        if not line.strip():
                            break
                        elif line.strip() == 'X          CP':
                            True
                        else:
                            strs = line.split()
                            if len(strs) == 2:
                                Xlower.append(float(strs[0]))
                                CPlower.append(float(strs[1]))
    except FileNotFoundError:
        print("File does not exist")
    
    return Xupper, CPupper, Xlower, CPlower, em, cl, cdv

def plot_cp(filename):
    Xupper, CPupper, Xlower, CPlower, em, cl, cdv = airload(filename)
    mach = float(em)
    # Begin by calculating Cp*
    cpstar = 2/1.4/mach/mach*(((2+0.4*mach*mach)/2.4)**3.5-1)
    # plt.plot(Xuold, CPuold, 'g', Xlold, CPlold, 'g')
    # plt.hold(True)
    plt.plot(Xupper, CPupper, 'r', Xlower, CPlower, 'r')
    plt.xlabel('x/c')
    plt.ylabel('-Cp')
    plt.axis([0, 1, -2, 1])
    plt.gca().invert_yaxis()
    plt.grid(True)
    plt.plot([0, 1], [cpstar, cpstar], 'b')
    plt.show()
    # Xuold = Xupper
    # Xlold = Xlower
    # CPuold = CPupper
    # CPlold = CPlower

def plot_cp_corr(filename):
    Xupper, CPupper, Xlower, CPlower, em, cl, cdv = airload(filename)
    mach = float(em)
    cpstar = 2/1.4/mach/mach*(((2+0.4*mach*mach)/2.4)**3.5-1)
    CPupper = [ max(cpstar, cp) for cp in CPupper ]; CPlower = [ max(cpstar, cp) for cp in CPlower ] # limit max to cpstar
    plt.plot(Xupper, CPupper, 'r', Xlower, CPlower, 'r')
    plt.xlabel('x/c')
    plt.ylabel('-Cp')
    plt.axis([0, 1, -2, 1])
    plt.gca().invert_yaxis()
    plt.grid(True)
    plt.plot([0, 1], [cpstar, cpstar], 'b')
    plt.show()