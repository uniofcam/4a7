import sys

in_file = str(sys.argv[1])
out_file = str(sys.argv[2])
factor = float(sys.argv[3])

with open(in_file, 'r') as input:
    with open(out_file, 'w') as output:
        for i, line in enumerate(input):
            if i < 2:
                output.write(line)
            else:
                columns = line.split()
                # columns[1] = str(round(float(columns[1]) * factor, len(columns[1].split('.')[1])))
                columns[0] = "{:.6f}".format(float(columns[0])*factor)
                columns[1] = "{:.6f}".format(float(columns[1])*factor)
                output.write(' '.join(columns) + '\n')
