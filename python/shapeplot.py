import numpy as np
import matplotlib.pyplot as plt
import sys

file = sys.argv[1]

# Load data from file
data = np.loadtxt(file, skiprows=2)
xu = data[:65, 0]
xl = data[65:, 0]
yu = data[:65, 1]
yl = data[65:, 1]

# Plot upper and lower surfaces
plt.plot(xu, yu)
plt.plot(xl, yl)

# Set axis limits and aspect ratio
plt.axis([0, 1, -0.5, 0.5])
plt.axis('equal')

# Show plot
plt.show()