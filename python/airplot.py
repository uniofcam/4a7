#
# Script file for transonic airfoil design exercise
#

from airload import airload
from airload import plot_cp
import sys

global Xupper, CPupper, Xlower, CPlower, em, cl, cdv

file = sys.argv[1]

Xupper, CPupper, Xlower, CPlower, em, cl, cdv = airload(file)
if cl != 0:
    print("CL: ", cl)
    if abs(cdv['cdv+cd1'] - cdv['cdv+cd1']) <= 0.04: # acceptable error
        print("CD2: ", cdv['cdv+cd2'] - cdv['cdv']) # use viscous cdv
    else:
        print("CD2 (inaccurate): ", cdv['cdv+cd2'] - cdv['cdv'])
    plot_cp(file)

#import numpy as np
#area = np.trapz(CPlower, Xlower) - np.trapz(CPupper, Xupper) 
#print("CL: ", area)
