import sys

sep = 0.02
base_file = str(sys.argv[1])

camber = {}
with open(base_file, 'r') as input:
    for i, line in enumerate(input):
        if i >= 2:
            columns = [float(i) for i in line.split()]
            if columns[0] > sep and columns[0] < 0.8 - sep:
                if len(columns) == 0:
                    True
                elif str(columns[0]) not in camber:
                    camber[str(columns[0])] = columns[1]
                elif str(columns[0]) in camber:
                    # if (columns[1] + camber[str(columns[0])]) / 2 > 0.0:
                        print('<point x="{:.6f}" y="{:.6f}" locked="0" />'.format(columns[0],(columns[1] + camber[str(columns[0])]) / 2))
                        camber[str(columns[0])] = (columns[1] + camber[str(columns[0])]) / 2
