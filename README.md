<!-- https://gitlab.developers.cam.ac.uk/js2597/4a7 -->
<!-- git checkout --orphan temp; git add -A; git commit -am "final"; git branch -D main; git branch -m main; git push -f origin main; git -c gc.reflogExpire=0 -c gc.reflogExpireUnreachable=0 -c gc.rerereresolved=0 -c gc.rerereunresolved=0 -c gc.pruneExpire=now gc "$@" -->
<!-- https://m-selig.ae.illinois.edu/ads/coord_database.html -->

# 4A7
### Scripts for automating VGK solver, see ESDU 96028

## Instructions
`./run.sh e` to populate all results to be analysed (requires Linux x86_64)

<!-- `python python/read.py` to find optimal supercritical aerofoil solution -->
`python/calc.ipynb` to generate plots and analysis

## Helpers
`python python/airplot.py [*.BRF]` to plot cp v. x/c 

<!-- `python python/camber.py [*.dat]` to calculate camber line -->

`python python/geom.py [in.dat] [out.dat] [factor]` to modify t/c for selected series

`python python/shapeplot.py [in.dat]` to plot shape of aerofoil

## Credits
University of Cambridge for pre-compiled VGK GTK frontend
