load test.dat   
% saves everything in a 2-d array called run (130,2)

xu=test(1:65,1);
xl=test(66:130,1);
yu=test(1:65,2);
yl=test(66:130,2);

%Now you have all necessary variable you can plot the shape:
% plotting upper surface
plot(xu,yu)     
hold on
% plotting lower surface
plot(xl,yl)
% setting the axis to be of equal range
axis([0 1 -.5 .5])
% making sure the axis are of equal length and invisible
axis off square

