%
%  Script file for transonic airfoil design exercise
%
%
global Xupper CPupper Xlower CPlower em cl cdv CPuold CPlold Xuold ...
    Xlold

airload Autorun.BRF

mach=str2num(em);

% Begin by calculating Cp*
cpstar = 2/1.4/mach/mach*(((2+0.4*mach*mach)/2.4)^3.5-1);

hold off
plot(Xuold,CPuold,'g',Xlold,CPlold,'g');
hold on;
plot(Xupper,CPupper,'r',Xlower,CPlower,'r');
xlabel('x/c');
ylabel('-Cp');
axis([0,1,-2,1]);
set (gca,'Ydir','reverse');
axis(axis);
grid on;
plot([0,1],[cpstar,cpstar],'b');
Xuold=Xupper;
Xlold=Xlower;
CPuold=CPupper;
CPlold=CPlower;



