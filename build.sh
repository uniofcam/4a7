sudo mkdir -p /usr/local/lib64/
sudo cp -r vgk /usr/local/lib64/airfoil-hb
# sudo rm -rf /usr/local/lib64/airfoil-hb

# 1. airfoil-nb generates Autorun.SIR
# 2. airfoil-nb runs vgk < Autorun.SIR

# for i in {1..999}; do
#     ps --forest -o pid,tty,stat,time,cmd -g $(ps -o sid= -p 2795)
# done

cp -r ~/Airfoil-hb/ test0 
mv ~/Airfoil-hb/ test.bak 
diff -qr test0 test.bak
ls -la --time-style=full-iso
rm -rf *.VEH *.SEP *.IEH *.FUL *.BRF

# CHGMACH, CHGR, CHGRE, CHGAOA
# change thickness modify run.dat for coordinates file
# python3 python/airplot.py results/rae5230-0.12/0.71-8.00-2.00/Autorun.BRF
# python3 python/geom.py test/run.rae5230.100.dat test/run.dat 0.12
# python3 python/shapeplot.py sample/run.rae5230.12.dat
# python3 python/reshape.ss.py sample/run.rae5230.12.dat sample/sc20712.dat
# python3 python/camber.py sample/rae5230.dat
